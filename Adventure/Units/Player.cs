﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Adventure.Units
{
    public enum PLAYER_CLASS
    {
        KNIGHT = 1,
        MAGE = 2,
        ARCHER = 3
    }

    public class Player : BaseUnit
    {
        // ## UNIQUE PROPERTIES ################################################################################
        public PLAYER_CLASS PlayerClass { get; set; }

        // ## OVERRIDES ########################################################################################
        public override int MinAttack { get { return base.MinAttack += (BonusAttack / 2); } }

        public override int MaxAttack { get { return base.MaxAttack += BonusAttack; } }

        public override int MaxMP { get { return base.MaxMP + BonusMP; } }

        public override int MPRegen { get { return base.MPRegen + (BonusMP / 2); } }

        public override int MaxHealth
        {
            get
            {
                switch (PlayerClass)
                {
                    case PLAYER_CLASS.KNIGHT:
                        return base.MaxHealth + 5;

                    case PLAYER_CLASS.MAGE:
                        return base.MaxHealth;

                    case PLAYER_CLASS.ARCHER:
                        return base.MaxHealth + 3;

                    default:
                        return base.MaxHealth;
                }
            }
        }

        public int BonusAttack
        {
            get
            {
                switch (PlayerClass)
                {
                    case PLAYER_CLASS.KNIGHT:
                        return 3;

                    case PLAYER_CLASS.MAGE:
                        return 0;

                    case PLAYER_CLASS.ARCHER:
                        return 1;

                    default:
                        return 0;
                }
            }
        }

        public int BonusMP
        {
            get
            {
                switch (PlayerClass)
                {
                    case PLAYER_CLASS.KNIGHT:
                        return 0;

                    case PLAYER_CLASS.MAGE:
                        return 4;

                    case PLAYER_CLASS.ARCHER:
                        return 2;

                    default:
                        return 0;
                }
            }
        }

        public int BonusMagicAttack
        {
            get
            {
                switch (PlayerClass)
                {
                    case PLAYER_CLASS.KNIGHT:
                        return 0;
                    case PLAYER_CLASS.MAGE:
                        return 3;
                    case PLAYER_CLASS.ARCHER:
                        return 1;
                    default:
                        return 0;
                }
            }
        }

        public override List<Actions.BaseAction> Actions
        {
            get
            {
                string weaponName = "";

                switch (PlayerClass)
                {
                    case PLAYER_CLASS.KNIGHT:
                        weaponName = "Black Knight Halberd";
                        break;

                    case PLAYER_CLASS.MAGE:
                        weaponName = "Magic staff";
                        break;

                    case PLAYER_CLASS.ARCHER:
                        weaponName = "Magic bow";
                        break;

                    default:
                        weaponName = "Unknown weapon";
                        break;
                }

                var result = new List<Actions.BaseAction>()
                {
                    new Actions.WeaponAttack(1, weaponName, MinAttack, MaxAttack),
                    new Actions.LightningBolt(2, bonusAttack: BonusMagicAttack)
                };

                return result;
            }
        }

        public void Init()
        {
            MP = MaxMP;
            Health = MaxHealth;
        }
    }
}