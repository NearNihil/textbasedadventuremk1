﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Adventure.Units
{
    public abstract class BaseUnit
    {
        public virtual int Id { get; set; }
        public virtual int Health { get; set; } = 10;
        public virtual int MaxHealth { get; set; } = 10;
        public virtual string Name { get; set; } = "Unnamed unit";
        public virtual int MinAttack { get; set; } = 4;
        public virtual int MaxAttack { get; set; } = 8;
        public virtual int Armor { get; set; } = 2;
        public virtual int MaxMP { get; set; } = 5;
        public virtual int MP { get; set; } = 5;
        public virtual int MPRegen { get; set; } = 2;
        public virtual List<Actions.BaseAction> Actions { get; } = new List<Actions.BaseAction>();

        public bool IsDead { get { return Health <= 0; } }

        public Random RNG { get { return new Random(); } }

        public string ActionsString
        {
            get
            {
                string result = "";

                foreach (var action in Actions)
                {
                    result += $"{action.Id}: {action.Name}\n";
                }

                return result;
            }
        }

        public virtual void RegenMP()
        {
            // if max is 5 and current is 4 and mp regen is 2, set to max
            if (MaxMP - MP < MPRegen)
            {
                MP = MaxMP;
            }
            // all other cases SHOULD be safe to just add the thing
            else
            {
                MP += MPRegen;
            }
        }

        public void ReceiveAction(Actions.BaseAction action)
        {
            int damage = RNG.Next(action.MinDamage, action.MaxDamage + 1);

            damage -= action.IsMagicAttack ? 0 : Armor;

            Health -= damage;

            Console.WriteLine($"{this.Name} takes a hit from {action.Name} and takes {damage}DMG! Down to {Health}HP.");
        }

        public override string ToString()
        {
            string abilitiesString = string.Join('\n', Actions);

            return $@"[{Name}] HP: {Health}/{MaxHealth} MP: {MP}/{MaxMP} Attack: {MinAttack}-{MaxAttack}
Abilities: {abilitiesString}
";
        }
    }
}