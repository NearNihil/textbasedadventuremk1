﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Adventure.Units.Monsters
{
    public class Goblin : BaseMonster
    {
        public Goblin(int Id) : base(Id) { }

        public override string Name { get => $"Goblin {Id}"; set => this.Name = value; }
    }
}
