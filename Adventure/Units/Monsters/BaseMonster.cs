﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Adventure.Units.Monsters
{
    public abstract class BaseMonster : BaseUnit
    {
        public BaseMonster(int Id)
        {
            this.Id = Id;
        }

        public override int Id { get; set; }

        public override string Name { get; set; } = "Evil monster";
    }
}
