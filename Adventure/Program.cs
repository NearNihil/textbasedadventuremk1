﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Adventure
{
    internal static class Program
    {
        public static Units.Player Player { get; set; } = new Units.Player();
        public static Units.Waifu Waifu { get; set; } = new Units.Waifu();
        public static List<Units.BaseUnit> PlayerSide { get { return new List<Units.BaseUnit>() { Player, Waifu }; } }

        public static void Main(string[] args)
        {
            Messages.ClearAndWriteMessage("Welcome to World of Waifu!\nYour name?");
            Player.Name = Console.ReadLine();

            Messages.ClearAndWriteMessage($"Well met {Player.Name}! What is your class? Your options are:");

            // Get a list of the enums instead of it being an enum
            var playerClassList = Enum.GetValues(typeof(Units.PLAYER_CLASS)).Cast<Units.PLAYER_CLASS>().ToList();

            foreach (Units.PLAYER_CLASS playerClass in playerClassList)
            {
                Console.WriteLine($"{(int)playerClass}: {playerClass.ToString()}");
            }

            int choice = 0;

            while (choice == 0 || choice > (int)playerClassList.Max())
            {
                int.TryParse(Console.ReadLine(), out choice);

                if (choice > 0 && choice <= (int)playerClassList.Max())
                {
                    Player.PlayerClass = (Units.PLAYER_CLASS)choice;
                    Player.Init();
                    Console.WriteLine($"Very well. You shall be known as {Player.Name} the honorable {((Units.PLAYER_CLASS)choice).ToString()}!");
                }
                else
                {
                    Console.WriteLine("That is not a valid choice. Try again.");
                }
            }

            new Encounters.Combat(2, false, "the Forest");

            Console.ReadLine();
        }
    }

    internal static class Messages
    {
        public static void ClearAndWriteMessage(string newMessage)
        {
            Console.Clear();
            Console.SetCursorPosition(0, 0);
            Console.WriteLine(newMessage);
        }
    }
}
