﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Adventure.Actions
{
    public abstract class BaseAction
    {
        public BaseAction(int Id, string Name)
        {
            this.Id = Id;
            this.Name = Name;
        }

        /// <summary>
        /// When rolling for or selecting an action, this is the thing to pick
        /// </summary>
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual int MPCost { get; set; } = 0;

        public virtual int MaxDamage { get; set; } = 0;

        public virtual int MinDamage { get; set; } = 0;

        public virtual bool IsMagicAttack { get; set; } = true;

        public override string ToString()
        {
            return $"[{Name}]: {(IsMagicAttack ? $"{MPCost} MP / " : "")} {MinDamage}-{MaxDamage} DMG";
        }
    }
}