﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Adventure.Actions
{
    public class WeaponAttack : BaseAction
    {
        public WeaponAttack(int Id, string Name, int MinAttack, int MaxAttack) : base (Id, Name)
        {
            this.Name = Name;
            this.MinDamage = MinAttack;
            this.MaxDamage = MaxAttack;
            this.IsMagicAttack = false;
        }
    }
}
