﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Adventure.Actions
{
    public class LightningBolt : BaseAction
    {
        public LightningBolt(int Id, string Name = "Lightning Bolt", int bonusAttack = 0, int mpModifier = 0) : base(Id, Name)
        {
            this.Id = Id;
            this.Name = Name;

            MaxDamage += bonusAttack;
            MinDamage += bonusAttack / 2;

            MPCost += mpModifier;
        }

        public override int MPCost { get; set; } = 5;

        public override int MinDamage { get; set; } = 5;

        public override int MaxDamage { get; set; } = 10;
    }
}
