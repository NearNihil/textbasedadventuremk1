﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adventure.Encounters
{
    public class Combat
    {
        public Combat(int enemyCount, bool boss, string environment)
        {
            Environment = environment;
            bool combatEnded = false;

            for (int i = 1; i <= enemyCount; i++)
            {
                Opponents.Add(new Units.Monsters.Goblin(i));
            }

            Console.WriteLine();
            Console.WriteLine($"{enemyCount} monster{(enemyCount != 1 ? "s" : "")} appear in {environment}! What will you do, {Program.Player.Name}?");
            Console.WriteLine();
            while (!combatEnded)
            {
                bool pickedLegalAction = false;
                int actionId = 0;

                while (!pickedLegalAction)
                {
                    Console.WriteLine(Program.Player.ActionsString);
                    string input = Console.ReadLine();

                    if (!int.TryParse(input, out actionId))
                    {
                        Console.WriteLine("Not a valid action! Try again.");
                    }
                    else
                    {
                        pickedLegalAction = true;
                    }
                }

                var action = Program.Player.Actions.Find(x => x.Id == actionId);
                Units.BaseUnit target;

                if (Opponents.Count == 1)
                {
                    target = Opponents[0];
                }
                else
                {
                    int targetId = 0;
                    while (targetId == 0 || targetId > Opponents.Max(x => x.Id))
                    {
                        Console.WriteLine("Select target:");
                        foreach (var badguy in Opponents)
                        {
                            Console.WriteLine($"{badguy.Id}: {badguy.Name} ({badguy.Health}HP)");
                        }

                        if (!int.TryParse(Console.ReadLine(), out targetId))
                        {
                            Console.WriteLine("Invalid target! Try again.");
                        }
                    }

                    target = Opponents.Find(x => x.Id == targetId);
                }

                target.ReceiveAction(action);
                Program.Player.MP -= action.MPCost;

                Console.WriteLine("On field:");
                Console.WriteLine("== Friendly ==");
                foreach (var item in Program.PlayerSide)
                {
                    Console.WriteLine(item.ToString());
                }
                Console.WriteLine("== Opponents ==");
                foreach (var item in Opponents)
                {
                    Console.WriteLine(item.ToString());
                }
            }
        }

        public string Environment { get; set; }

        public List<Units.BaseUnit> Opponents { get; set; } = new List<Units.BaseUnit>();
    }
}
